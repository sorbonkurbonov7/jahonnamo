let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
	'resources/assets/admin/bootstrap/css/bootstrap.min.css',
	'resources/assets/admin/font-awesome/4.5.0/css/font-awesome.min.css',
	'resources/assets/admin/ionicons/2.0.1/css/ionicons.min.css',
	'resources/assets/admin/plugins/iCheck/minimal/_all.css',
	'resources/assets/admin/plugins/datepicker/datepicker3.css',
	'resources/assets/admin/plugins/select2/select2.min.css',
	'resources/assets/admin/plugins/datatables/dataTables.bootstrap.css',
	'resources/assets/admin/dist/css/AdminLTE.min.css',
	'resources/assets/admin/dist/css/skins/_all-skins.min.css'
], 'public/css/admin.css');

mix.scripts([
	'resources/assets/admin/plugins/jQuery/jquery-2.2.3.min.js',
	'resources/assets/admin/bootstrap/js/bootstrap.min.js',
	'resources/assets/admin/plugins/select2/select2.full.min.js',
	'resources/assets/admin/plugins/datepicker/bootstrap-datepicker.js',
	'resources/assets/admin/plugins/datatables/jquery.dataTables.min.js',
	'resources/assets/admin/plugins/datatables/dataTables.bootstrap.min.js',
	'resources/assets/admin/plugins/slimScroll/jquery.slimscroll.min.js',
	'resources/assets/admin/plugins/fastclick/fastclick.js',
	'resources/assets/admin/plugins/iCheck/icheck.min.js',
	'resources/assets/admin/dist/js/app.min.js',
	'resources/assets/admin/dist/js/demo.js',
	'resources/assets/admin/dist/js/scripts.js'
], 'public/js/admin.js');

mix.copy('resources/assets/admin/bootstrap/fonts', 'public/fonts');
mix.copy('resources/assets/admin/dist/fonts', 'public/fonts');
mix.copy('resources/assets/admin/dist/img', 'public/img');
mix.copy('resources/assets/admin/plugins/iCheck/minimal/blue.png', 'public/img');



mix.styles([
	'resources/assets/front/css/bootstrap.min.css',
	'resources/assets/front/css/font-awesome.min.css',
	'resources/assets/front/css/animate.min.css',
	'resources/assets/front/css/owl.carousel.css',
	'resources/assets/front/css/owl.theme.css',
	'resources/assets/front/css/owl.transitions.css',
	'resources/assets/front/css/style.css',
	'resources/assets/front/css/responsive.css',
], 'public/css/front.css');

mix.scripts([
	'resources/assets/front/js/jquery-1.11.3.min.js',
	'resources/assets/front/js/bootstrap.min.js',
	'resources/assets/front/js/owl.carousel.min.js',
	'resources/assets/front/js/jquery.stickit.min.js',
	'resources/assets/front/js/menu.js',
	'resources/assets/front/js/scripts.js',
], 'public/js/front.js');

mix.copy('resources/assets/front/fonts', 'public/fonts');
mix.copy('resources/assets/front/images', 'public/images');

mix.styles([
	'resources/assets/contact/css/style.css',
], 'public/css/contact.css');

mix.scripts([
	'resources/assets/contact/js/jquery/jquery-2.2.4.min.js',
	'resources/assets/contact/js/bootstrap/popper.min.js',
	'resources/assets/contact/js/bootstrap/bootstrap.min.js',
	'resources/assets/contact/js/plugins/plugins.js',
	'resources/assets/contact/js/active.js',
], 'public/js/contact.js');


mix.styles([
	'resources/assets/front/onlineTV/css/demo.css',
	'resources/assets/front/onlineTV/css/quality.css'
], 'public/css/onlineTV.css');

mix.scripts([
	'resources/assets/front/onlineTV/js/quality.js',
], 'public/js/onlineTV.js');


mix.styles([
	'resources/assets/front/about/css/about.css', //
	'resources/assets/front/about/css/about_responsive.css',
	'resources/assets/front/about/css/bootstrap.min.css',
	'resources/assets/front/about/css/video-js.css',
], 'public/css/about.css');

mix.scripts([
	'resources/assets/front/about/js/about.js',
	'resources/assets/front/about/js/animation.gsap.min.js',
	'resources/assets/front/about/js/easing.js',
	'resources/assets/front/about/js/jquery-3.2.1.min.js',
	'resources/assets/front/about/js/parallax.min.js',
	'resources/assets/front/about/js/popper.js',
	'resources/assets/front/about/js/ScrollMagic.min.js',
	'resources/assets/front/about/js/ScrollToPlugin.min.js',
	'resources/assets/front/about/js/TimelineMax.min.js',
	'resources/assets/front/about/js/TweenMax.min.js',
], 'public/js/about.js');

mix.copy('resources/assets/front/about/images', 'public/images');












