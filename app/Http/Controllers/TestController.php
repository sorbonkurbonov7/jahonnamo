<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        $post = Post::paginate('2');
        return view('pages.test_index', compact('post', ''));
    }
}
