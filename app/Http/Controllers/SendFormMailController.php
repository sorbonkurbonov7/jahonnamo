<?php

namespace App\Http\Controllers;

use App\Mail\SendFormMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class SendFormMailController extends Controller
{

    public function send_form(Request $request)
    {
        $this->validate($request, [
            'name'     =>  'required',
            'email'  =>  'required|email',
            'phone' =>  'required',
            'message' =>  'required'
        ]);


        $data = array(
            $name = $request->name,
            $email = $request->email,
            $phone = $request->phone,
            $message =  $request->message,
        );

        Mail::to('sorkurbonov@yandex.ru')->send(new SendFormMail($data));

        return redirect('/contact');
    }
}
