<?php

namespace App\Providers;

use App\Comment;
use App\Post;
use App\Category;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      view()->composer('pages._sidebar', function ($view){

        $view->with('popularPost', Post::getPopularPosts());

        $view->with('featuredPosts', Post::where
        ('is_featured', 1)->take(3)->get());

        $view->with('recentPosts', Post::orderBy
        ('date', 'desc')->take(3)->get());

        $view->with('categories', Category::all());
    });

      view()->composer('admin._sidebar', function ($view){

        $view->with('newsCommentCount', Comment::where('status', 0
        )->count());

      });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
