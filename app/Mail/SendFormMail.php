<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendFormMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
//    protected $email;
//    protected $phone;
//    protected $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
//        $this->email = $email;
//        $this->phone = $phone;
//        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('contact.action.email_contact')
            ->with([
                'data' => $this->data,
//                'email' => $this->email,
//                'phone' => $this->phone,
//                'message' => $this->message
            ])
            ->subject('Новое письмо');
    }
}
