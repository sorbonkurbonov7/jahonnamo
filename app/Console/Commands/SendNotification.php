<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use App\User;
use App\mail\SendEmail;
use Illuminate\Support\Facades\Mail;


class SendNotification  extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send {--id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending emails to the users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->info('Started sending emails...');

      $data = array(
          'name' => "Alif capital', гр. Душанбе",
      );

      Mail::send('dynamic_email_template', $data, function ($message) {

          $message->from('sorbonkurbonov7@yandex.ru', 'МГ "Alif Shop"');

          $message->to('sorbonkurbonov7@mail.ru')->subject('Покупки');

      });
      $this->info('The emails are send successfully!');
    }

}























