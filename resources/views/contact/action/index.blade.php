@extends('contact.layout')
@section('content')

  <!-- ##### Breadcrumb Area Start ##### -->
  <div class="breadcrumb-area">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/"><i class="fa fa-home"></i> Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Contact</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Breadcrumb Area End ##### -->

  <!-- ##### Google Maps Start ##### -->
  <div class="map-area">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3118.57906893104!2d68.78092071499869!3d38.58954007961932!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38b5d3c3310bc085%3A0xc531a983092ad783!2z0J7QkNCeINCi0LXQu9C10YDQsNC00LjQvtC60L7QvA!5e0!3m2!1sru!2s!4v1589874038392!5m2!1sru!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
  </div>
  <!-- ##### Google Maps End ##### -->

  <!-- ##### Contact Area Start ##### -->
  <section class="contact-area section-padding-100-0">
    <div class="container">
      <div class="row">

        <div class="col-12 col-lg-6">
          <div class="contact-content mb-100">
            <h4>Get In Touch</h4>
            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia dese mollit anim id est laborum, consectetur adipisicing elit.</p>
            <!-- Single Contact Info -->
            <div class="single-contact-info">
              <h6>Location:</h6>
              <h4>40 Baria Sreet 133/2 NewYork, US</h4>
            </div>

            <!-- Single Contact Info -->
            <div class="single-contact-info">
              <h6>Email:</h6>
              <h4>info.hello@gmail.com</h4>
            </div>

            <!-- Single Contact Info -->
            <div class="single-contact-info">
              <h6>Phone:</h6>
              <h4>(+992) 456-789-1120</h4>
            </div>
          </div>
        </div>

        <div class="col-12 col-lg-6">
          <div class="contact-content mb-100">
            <h4>Contact Form</h4>

            <!-- Contact Form Area -->
            <div class="contact-form-area">
{{--              <form action="" method="post"}}>--}}
{{--                {{csrf_field()}}--}}
                {{Form::open([
                  'route' => 'send.form',
                ])}}
                <div class="form-group">
                  <input type="text" class="form-control" id="contact-name" placeholder="Name" name="name">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="contact-email" placeholder="Email" name="email">
                </div>
                <div class="form-group">
                  <input type="number" class="form-control" id="contact-phone" placeholder="Phone" name="phone">
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
                </div>
                <button type="submit" class="btn nikki-btn mt-15">Submit</button>
              {{Form::close()}}
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- ##### Contact Area End ##### -->

@endsection