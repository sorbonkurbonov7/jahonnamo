@extends('contact.layout')

@section('style')
    <style rel="stylesheet" href="/css/about.css"></style>
@endsection

@section('content')
    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/"><i class="fa fa-home"></i> Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">About</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="super_container" style="background-color: #DDD;">
        <!-- About -->
        <div class="about">
            <div class="container">
                <div class="row about_row row-lg-eq-height">
                    <div class="col-lg-6">
                        <div class="about_content">
                            <div class="about_title">Our Platform's main goal</div>
                            <div class="about_text">
                                <p>Suspendisse tincidunt magna eget massa hendrerit efficitur. Ut euismod pellentesque imperdiet. Cras laoreet gravida lectus, at viverra lorem venenatis in. Aenean id varius quam. Nullam bibendum interdum dui, ac tempor lorem convallis ut. Maecenas rutrum viverra sapien sed fermentum. Morbi tempor odio eget lacus tempus pulvinar. Praesent vel nisl fermentum, gravida augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id convallis libero, sed blandit nibh. Nam ultricies tristique nibh, consequat ornare nibh. Quisque vitae odio ligula.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about_image"><img src="images/about_1.jpg" alt="https://unsplash.com/@jtylernix"></div>
                    </div>
                </div>
                <div class="row about_row row-lg-eq-height">
                    <div class="col-lg-6 order-lg-1 order-2">
                        <div class="about_image"><img src="images/about_1.jpg" alt=""></div>
                    </div>
                    <div class="col-lg-6 order-lg-2 order-1">
                        <div class="about_content">
                            <div class="about_title">eLearn's Vision</div>
                            <div class="about_text">
                                <p>Suspendisse tincidunt magna eget massa hendrerit efficitur. Ut euismod pellentesque imperdiet. Cras laoreet gravida lectus, at viverra lorem venenatis in. Aenean id varius quam. Nullam bibendum interdum dui, ac tempor lorem convallis ut. Maecenas rutrum viverra sapien sed fermentum. Morbi tempor odio eget lacus tempus pulvinar. Praesent vel nisl fermentum, gravida augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id convallis libero, sed blandit nibh. Nam ultricies tristique nibh.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="teachers">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="teachers_title text-center">Meet the Teachers</div>
                    </div>
                </div>
                <div class="row teachers_row">

                    <!-- Teacher -->
                    <div class="col-lg-4 col-md-6">
                        <div class="teacher">
                            <div class="teacher_image"><img src="images/teacher_1.jpg" alt="https://unsplash.com/@nickkarvounis"></div>
                            <div class="teacher_body text-center">
                                <div class="teacher_title"><a href="#">Jonathan Smith</a></div>
                                <div class="teacher_subtitle">Marketing</div>
                                <div class="teacher_social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Teacher -->
                    <div class="col-lg-4 col-md-6">
                        <div class="teacher">
                            <div class="teacher_image"><img src="images/teacher_2.jpg" alt="https://unsplash.com/@rawpixel"></div>
                            <div class="teacher_body text-center">
                                <div class="teacher_title"><a href="#">Michelle Williams</a></div>
                                <div class="teacher_subtitle">Art & Design</div>
                                <div class="teacher_social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Teacher -->
                    <div class="col-lg-4 col-md-6">
                        <div class="teacher">
                            <div class="teacher_image"><img src="images/teacher_3.jpg" alt="https://unsplash.com/@taylor_grote"></div>
                            <div class="teacher_body text-center">
                                <div class="teacher_title"><a href="#">Jack Gallagan</a></div>
                                <div class="teacher_subtitle">Marketing</div>
                                <div class="teacher_social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Teacher -->
                    <div class="col-lg-4 col-md-6">
                        <div class="teacher">
                            <div class="teacher_image"><img src="images/teacher_4.jpg" alt="https://unsplash.com/@benjaminrobyn"></div>
                            <div class="teacher_body text-center">
                                <div class="teacher_title"><a href="#">Christinne Smith</a></div>
                                <div class="teacher_subtitle">Marketing</div>
                                <div class="teacher_social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Teacher -->
                    <div class="col-lg-4 col-md-6">
                        <div class="teacher">
                            <div class="teacher_image"><img src="images/teacher_5.jpg" alt="https://unsplash.com/@christinhumephoto"></div>
                            <div class="teacher_body text-center">
                                <div class="teacher_title"><a href="#">Michelle Williams</a></div>
                                <div class="teacher_subtitle">Art & Design</div>
                                <div class="teacher_social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Teacher -->
                    <div class="col-lg-4 col-md-6">
                        <div class="teacher">
                            <div class="teacher_image"><img src="images/teacher_6.jpg" alt="https://unsplash.com/@rawpixel"></div>
                            <div class="teacher_body text-center">
                                <div class="teacher_title"><a href="#">Jack Gallagan</a></div>
                                <div class="teacher_subtitle">Marketing</div>
                                <div class="teacher_social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center">
                        <div class="button teachers_button"><a href="#">see all teachers<div class="button_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/about.js"></script>
@endsection
