@extends('layout')

@section('style')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.6/mediaelementplayer.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">
  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
  <link rel="stylesheet" href="/css/video.css">
  <link rel="stylesheet" href="/css/slider.css">
  <link rel="stylesheet" href="/css/grid.css">
@endsection()

@section('content')
  <!--main content start-->
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="cats white p-projects">
            <div class="text"><a href="/projects">Праямой эфир</a></div>
            <div class="corner"></div>
          </div>
          <div class="media-wrapper">
            <video id="player1" width="381" height="300" class="mt-0" controls preload="none" poster="http://mediaelementjs.com/images/big_buck_bunny.jpg">
              <source type="video/mp4" src="http://teleradiocom.tj/online-tv/" data-quality="LD">
            </video>
          </div>
        </div>
        <div class="col-md-4">
          @include('pages.slider')
        </div>
          @include('pages._sidebar', ['posts', '$posts'] )
        <div class="col-md-8" style="position: absolute; margin-top: 400px">
          @foreach($posts as $post)
            <div class="cats white p-projects">
              <div class="text"><a href="/projects">Новости</a></div>
              <div class="corner"></div>
            </div>
            <article class="post">
              <div class="post-thumb">
                <a href="{{route('post.show', $post->slug)}}"><img src="{{$post->getImage()}}" alt=""></a>

                <a href="{{route('post.show', $post->slug)}}" class="post-thumb-overlay text-center">
                  <div class="text-uppercase text-center">View Post</div>
                </a>
              </div>
              <div class="post-content">
                <header class="entry-header text-center text-uppercase">
                  @if($post->hasCategory())
                    <h6><a href="{{route('category.show', $post->category->slug)}}">{{$post->getCategoryTitle()}}</a></h6>
                  @endif
                  <h1 class="entry-title"><a href="{{route('post.show', $post->slug)}}">{{$post->title}}</a></h1>
                </header>
                <div class="entry-content">
                  {!! $post->description !!}
                  <div class="btn-continue-reading text-center text-uppercase">
                    <a href="{{route('post.show', $post->slug)}}" class="more-link">Continue Reading</a>
                  </div>
                </div>
                <div class="social-share">
            <span class="social-share-title pull-left text-capitalize">
              <a href="#"></a>  {{$post->getDate()}}</span>
                  <ul class="text-center pull-right">
                    <li><a class="s-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="s-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="s-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a class="s-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a class="s-instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                  </ul>
                </div>
              </div>
            </article>
          @endforeach
          {{$posts->links()}}
        </div>
        <div class="col-md-8" id="grid_pagination">
          <div class="cats white p-projects">
            <div class="text"><a href="#">Программы</a></div>
            <div class="corner"></div>
          </div>
          <div class="programms-container">
            <a href="#" class="programm-link">
              <img src="/images/gird.png" alt="">
            </a>
            <a href="#" class="programm-link">
              <img src="/images/gird.png" alt="">
            </a>
            <a href="#" class="programm-link">
              <img src="/images/gird.png" alt="">
            </a>
            <a href="#" class="programm-link">
              <img src="/images/gird.png" alt="">
            </a>
            <a href="#" class="programm-link">
              <img src="/images/gird.png" alt="">
            </a>
            <a href="#" class="programm-link">
              <img src="/images/gird.png" alt="">
            </a>
            <a href="#" class="programm-link">
              <img src="/images/gird.png" alt="">
            </a>
            <a href="#" class="programm-link">
              <img src="/images/gird.png" alt="">
            </a>
          </div>
        </div>
      </div>
    </div>
  <!-- end main content-->
@endsection()

@section('scripts')
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.6/mediaelement-and-player.min.js"></script>
  <script src="/js/onlineTV.js"></script>

  <script>
    var mediaElements = document.querySelectorAll('video, audio');
    for (var i = 0, total = mediaElements.length; i < total; i++) {
      new MediaElementPlayer(mediaElements[i], {
        features: ['playpause', 'current', 'progress', 'duration', 'volume', 'quality', 'fullscreen'],
      });
    }

    if(Hls.isSupported())
    {
      var video = document.getElementById('video');
      var hls = new Hls();
      hls.loadSource('http://185.121.2.6:8081/live/tvt1214/playlist.m3u8');
      hls.attachMedia(video);
      hls.on(Hls.Events.MANIFEST_PARSED,function()
      {

      });
    }
    else if (video.canPlayType('application/vnd.apple.mpegurl'))
    {
      video.src = 'http://185.121.2.6:8081/live/tvt1214/playlist.m3u8';
      video.addEventListener('canplay',function()
      {

      });
    }


    $(function() {

      // Menu Tabular
      var $menu_tabs = $('.menu__tabs li a');
      $menu_tabs.on('click', function(e) {
        e.preventDefault();
        $menu_tabs.removeClass('active');
        $(this).addClass('active');

        $('.menu__item').fadeOut(300);
        $(this.hash).delay(300).fadeIn();
        video.pause();
        videoq.pause();
        videow.pause();
        videoe.pause();

      });

    });

  </script>
@endsection()