
<div class="cats white p-projects">
  <div class="text"><a href="/projects">Программы</a></div>
  <div class="corner"></div>
</div>
<!-- Slider main container -->
<div class="swiper-container">
  <!-- Additional required wrapper -->
  <div class="swiper-wrapper">
    <!-- Slides -->
    <div class="swiper-slide">
      <a href="https://github.com/">
        <img src="/images/slider1.png" alt="">
      </a>
    </div>
    <div class="swiper-slide">
      <a href="https://github.com/">
        <img src="/images/slider2.png" alt="">
      </a>
    </div>
    <div class="swiper-slide">
      <a href="https://github.com/">
        <img src="/images/slider1.png" alt="">
      </a>
    </div>
  </div>
  <!-- If we need pagination -->
  <div class="swiper-pagination"></div>

  <!-- If we need navigation buttons -->
  <div class="swiper-button-prev"></div>
  <div class="swiper-button-next"></div>
</div>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script>
    let mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        loop: true,
        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    })
</script>